﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gym2 {
  public static class Users {
    public static IEnumerable<User> GetUsers() {
      var ctx = new GymDbContext();
      return
        from u in ctx.Users
        orderby u.Surname, u.Name
        select u;
    }
  }
}
