﻿namespace Gym2 {
  partial class FormEntrances {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.lbEntrances = new System.Windows.Forms.ListBox();
      this.btnInsert = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lbEntrances
      // 
      this.lbEntrances.FormattingEnabled = true;
      this.lbEntrances.Location = new System.Drawing.Point(12, 12);
      this.lbEntrances.Name = "lbEntrances";
      this.lbEntrances.Size = new System.Drawing.Size(390, 368);
      this.lbEntrances.TabIndex = 0;
      // 
      // btnInsert
      // 
      this.btnInsert.Location = new System.Drawing.Point(12, 398);
      this.btnInsert.Name = "btnInsert";
      this.btnInsert.Size = new System.Drawing.Size(390, 40);
      this.btnInsert.TabIndex = 1;
      this.btnInsert.Text = "Zabilježi";
      this.btnInsert.UseVisualStyleBackColor = true;
      this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
      // 
      // FormEntrances
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(414, 450);
      this.Controls.Add(this.btnInsert);
      this.Controls.Add(this.lbEntrances);
      this.Name = "FormEntrances";
      this.Text = "FormEntrances";
      this.Load += new System.EventHandler(this.FormEntrances_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox lbEntrances;
    private System.Windows.Forms.Button btnInsert;
  }
}