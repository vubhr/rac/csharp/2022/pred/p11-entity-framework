﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gym2 {
  public static class Entrances {
    public static IEnumerable<Entrance> GetEntrances(User user) {
      var ctx = new GymDbContext();

      return
        from e in ctx.Entrances
        where e.User.Id == user.Id
        orderby e.EnteredAt descending
        select e;
    }

    public static async void AddEntrance(User user) {
      var ctx = new GymDbContext();

      var entrance = new Entrance();
      entrance.UserId = user.Id;
      entrance.EnteredAt = DateTime.Now;

      ctx.Entrances.Add(entrance);
      await ctx.SaveChangesAsync();
    }
  }
}
