namespace Gym2 {
  using System;
  using System.Collections.Generic;
  using System.ComponentModel.DataAnnotations;
  using System.ComponentModel.DataAnnotations.Schema;
  using System.Data.Entity.Spatial;

  public partial class Entrance {
    public int Id { get; set; }

    public int? UserId { get; set; }

    public DateTime? EnteredAt { get; set; }

    public virtual User User { get; set; }

    public override string ToString() {
      return EnteredAt.ToString() + User.ToString();
    }
  }
}
