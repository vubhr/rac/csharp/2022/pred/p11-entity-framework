﻿namespace Gym2 {
  partial class FormUsers {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.lbUsers = new System.Windows.Forms.ListBox();
      this.SuspendLayout();
      // 
      // lbUsers
      // 
      this.lbUsers.FormattingEnabled = true;
      this.lbUsers.Location = new System.Drawing.Point(12, 12);
      this.lbUsers.Name = "lbUsers";
      this.lbUsers.Size = new System.Drawing.Size(365, 420);
      this.lbUsers.TabIndex = 0;
      this.lbUsers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbUsers_MouseDoubleClick);
      // 
      // FormUsers
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(393, 450);
      this.Controls.Add(this.lbUsers);
      this.Name = "FormUsers";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.FormUsers_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox lbUsers;
  }
}

