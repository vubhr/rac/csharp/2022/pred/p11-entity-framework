﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gym2 {
  public partial class FormUsers : Form {
    public FormUsers() {
      InitializeComponent();
    }

    private void FormUsers_Load(object sender, EventArgs e) {
      lbUsers.Items.AddRange(Users.GetUsers().ToArray());
    }

    private void lbUsers_MouseDoubleClick(object sender, MouseEventArgs e) {
      var user = (User)lbUsers.SelectedItem;
      FormEntrances formEntrances = new FormEntrances(user);
      formEntrances.ShowDialog();
    }
  }
}
