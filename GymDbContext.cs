using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Gym2 {
  public partial class GymDbContext : DbContext {
    public GymDbContext()
        : base("name=GymDbContext") {
    }

    public virtual DbSet<Entrance> Entrances { get; set; }
    public virtual DbSet<User> Users { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder) {
      modelBuilder.Entity<User>()
          .Property(e => e.Name)
          .IsUnicode(false);

      modelBuilder.Entity<User>()
          .Property(e => e.Surname)
          .IsUnicode(false);

      modelBuilder.Entity<User>()
          .Property(e => e.Card)
          .IsFixedLength();
    }
  }
}
