﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gym2 {
  public partial class FormEntrances : Form {
    public readonly User user;
    public FormEntrances() {
      InitializeComponent();
    }

    public FormEntrances(User user) : this() {
      this.user = user;
    }

    private void FormEntrances_Load(object sender, EventArgs e) {
      //MessageBox.Show(user.ToString());
      lbEntrances.Items.AddRange(Entrances.GetEntrances(user).ToArray());
    }

    private void btnInsert_Click(object sender, EventArgs e) {
      Entrances.AddEntrance(user);
      lbEntrances.Items.Clear();
      lbEntrances.Items.AddRange(Entrances.GetEntrances(user).ToArray());
    }
  }
}
